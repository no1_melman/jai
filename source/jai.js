(function(context) {

	// ---------------------------------------------- 
	// ----------- Dependency Dictionary ------------
	// ----------------------------------------------
	var dependencies = [];

	// -- Dependency Injector Container Interface ---	
	var _jai = {
		$resolve: $resolve,
		$get: $get,
		$register: $register,
		$run: $run
	};	

	// ---- Dependency Injector Exposure ----
	context.Jai = function () {
		dependencies = [];
	
		return _jai;
	};
	
	// ---------------------------------------------- 
	// ------------- Public DI Methods --------------
	// ----------------------------------------------
	
	/**
	 * @name jai.$resolve
	 * @kind function
	 *
	 * @description
	 * Resolves a service from the container and any child services,
	 * applying arguments to each function
	 *
	 *
	 * @param {Array | function} Usages:
	 *                           [{string}*, {function}] if you want child dependencies
	 *                                                   to be resolved
	 *                           {function}              if you just want to call the service
	 *
	 * @returns {function} execute this function when you want to execute the service
	 */
	function $resolve(service) {
		if (typeof service === 'function') {
			return function () { return service.apply({}); };
		}

		if ((service instanceof Array) === false) {
			throw new Error('Can\'t handle this object type');
		}

		var args = [];
		var actualService;

		for (var i = 0; i < service.length; i++) {
			var serviceArg = service[i];
			
			if (typeof serviceArg === 'string') {
				// we have a dependency
				var depService = $get(serviceArg); // get the service
				var resolveDepService = $resolve(depService)(); // resolve it and execute it
				args.push(resolveDepService);
			} else if (typeof serviceArg === 'function') {
				actualService = serviceArg; // this is the actual service
				break;
			}
		}

		return function() {
		  return actualService.apply({}, args.concat(Array.prototype.slice.call(arguments, 0)));
		}
	}

	/**
	 * @name jai.$get
	 * @kind function
	 *
	 * @description
	 * Retrieves a service from the dependencies container
	 * The container is a dictionary with Key: {string} service name, 
	 * Value: {Array | function} service function and/or dependencies
	 *
	 *
	 * @param {string} Service name that is stored in the dependency container
	 *
	 * @returns {Array | function} Container value, so service function and/or dependencies
	 */
	function $get(serviceName) {
		return dependencies[serviceName];
	}

	/**
	 * @name jai.$register
	 * @kind function
	 *
	 * @description
	 * Registers a dependency into the container
	 *
	 *
	 * @param {string} Service name that is stored in the dependency container
	 *
	 * @param {Array | function} Container value, so service function and/or dependencies
	 */
	function $register(dependencyName, dependencyArray) {
		dependencies[dependencyName] = dependencyArray;
	}
	
	/**
	 * @name jai.$run
	 * @kind function
	 *
	 * @description
	 * This is to execute the application so it's effectively the bootstrap function
	 * Here you want to pass in the name of your bootstrapping service 
	 *
	 *
	 * @param {string} Service name that is stored in the dependency container
	 */
	function $run(serviceName) {
		var service = $get(serviceName);
		
		$resolve(service)();
	}
})(this); 
